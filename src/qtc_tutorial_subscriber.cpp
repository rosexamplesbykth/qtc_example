#include "ros/ros.h"
//#include "std_msgs/String.h"

#include "hello_qtc/Msg_qtc_tutorial.h"

void chatterCallback(const hello_qtc::Msg_qtc_tutorial::ConstPtr& msg)
{
  ROS_INFO("receive msg = %d", msg->stamp.sec);
  ROS_INFO("receive msg = %d", msg->stamp.nsec);
  //ROS_INFO("I heard: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "qtc_tutorial_subscriber");
  ros::NodeHandle nh;

  ros::Subscriber sub = nh.subscribe("chatter", 1000, chatterCallback);

  ros::spin();

  return 0;
}
